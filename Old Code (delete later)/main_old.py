import Bio
from Bio import GenBank, SeqIO, AlignIO
from Bio.Seq import  Seq
from Bio.Data import CodonTable
from Bio.Alphabet import generic_dna
import csv
import glob
import pandas as pd
import os
import csv

# TO-DO:
# Controllare se ci sono cancellazioni/inserimenti nelle CDS
# Si riesce a far andare find_genes sui dati clean?


"""
def find_variations(tool):
    s = ""
    r = ""
    l = 0
    k = []    
    for f in glob.glob('Results/'+tool+'/*.fasta'):
        aln = list(AlignIO.read(open(f), "fasta"))        
        for i in range(1, len(aln)):
            diffs = []
            for j in range(len(aln[i].seq)):
                if aln[i].seq[j].upper() != aln[0].seq[j].upper():
                    find_genes(j,aln[0],aln[i])
                    s += aln[i].seq[j]
                    r += aln[0].seq[j]
                    l += 1
                    if (j == len(aln[i].seq)-1):
                        if "Italy" not in aln[i].id:
                            index = j - l + 1
                        else:
                            index = j-l
                        element =  [aln[i].id.split('/')[0], index, l, r, s]
                        k.append(element)
                        s = ""
                        r = ""
                        l = 0
                else:
                    if s != "" and r != "":
                        if "Italy" not in aln[i].id:
                            index = j - l + 1
                        else:
                            index = j-l
                        element =  [aln[i].id.split('/')[0], index, l, r, s]                       
                        for position in range(j - l, j+1) :
                            if aln[i].seq[position].upper() != aln[0].seq[position].upper():
                                diff = find_genes(position, aln[0], aln[i])
                                if diff and not isIn(diff, diffs):
                                    diffs.append(diff)
                        k.append(element)
                        s = ""
                        r = ""
                        l = 0
            if diffs:
                for w in diffs:
                    for q in w: #ottengo le singole tuple
                        print(q, file=open(tool+"-genes.txt", "a"))                    

"""

def find_genes(b,ref,seq):
    gb_file = "sequence.gb"
    result = []
    ref = ref.upper()
    seq = seq.upper()

    for gb_record in SeqIO.parse(open(gb_file,"r"), "genbank") :
        for features in gb_record.features :
            if (features.type == "CDS"): 
                if b in features.location: # intervallo CDS
                    p = b - min(features.location) + 1 
                    positions = []
                    
                    if p%3 == 0:
                        cod = ref[b-2]+ref[b-1]+ref[b] #codone originale
                        m_cod = seq[b-2]+seq[b-1]+seq[b]
                        # Check adiacent positions
                        if ref[b-2] != seq[b-2]:
                            positions.append(b-2)
                        if ref[b-1] != seq[b-1]:
                            positions.append(b-1)
                        positions.append(b)

                    elif p%3 == 1 and (b+2) < max(features.location):
                        cod = ref[b]+ref[b+1]+ref[b+2]
                        m_cod = seq[b]+seq[b+1]+seq[b+2]
                        positions.append(b)
                        if ref[b+1] != seq[b+1]:
                            positions.append(b+1)
                        if ref[b+2] != seq[b+2]:
                            positions.append(b+2)
                        
                    elif p%3 == 2 and (b+1) < max(features.location):
                        cod = ref[b-1]+ref[b]+ref[b+1]
                        m_cod = seq[b-1]+seq[b]+seq[b+1]                       
                        if ref[b-1] != seq[b-1]:
                            positions.append(b-1)
                        positions.append(b)
                        if ref[b+1] != seq[b+1]:
                            positions.append(b+1)

                    e = seq.id, positions, features.qualifiers['db_xref'][0], cod, translate(cod), m_cod, translate(m_cod), min(features.location), max(features.location)
                    if "N" not in m_cod:
                        result.append(e)
    if result: 
        return result

def isIn(item, listIn):
    
    itemIn = item[0]
    if len(listIn) == 0:
        return False

    element = len(listIn)
    for i in range(element) :
        for it in listIn[i]:
            bOut = True

            # Seq name
            bOut = bOut and itemIn[0] == it[0]    

            # Position
            i = 0
            for pos in itemIn[1]:
                bOut = bOut and pos == itemIn[1][i]
                i += 1
            
            # GeneID
            bOut = bOut and itemIn[2] == it[2]

            # Ref
            bOut = bOut and itemIn[3] == it[3]

            # Seq
            bOut = bOut and itemIn[5] == it[5]

            # Min
            bOut = bOut and itemIn[7] == it[7]

            # Max
            bOut = bOut and itemIn[8] == it[8]

            if bOut:
                return True

    return False

def translate(codon): 

    codon = Seq(codon, generic_dna) # formato Seq
    rna_cod = str(codon.transcribe()) # diventa RNA

    table = CodonTable.standard_rna_table.forward_table

    amino = table.get(rna_cod)
    return amino
    
def find_variations(country):
    s = []
    r = []
    l = 0
    var = []
    url = 'Results/*' + country + '.fasta'
    for f in glob.glob(url):
        tool = os.path.split(f)[-1].split("-")[0]
        aln = list(AlignIO.read(open(f), "fasta")) 
        for i in range(1, len(aln)):
            diffs = []
            for j in range(len(aln[i].seq)):
                if aln[i].seq[j].upper() != aln[0].seq[j].upper():
                    find_genes(j,aln[0],aln[i])
                    s += aln[i].seq[j].upper()
                    r += aln[0].seq[j].upper()
                    l += 1
                    if (j == len(aln[i].seq)-1):
                        if country != 'italy':
                            index = j - l + 1
                        else:
                            index = j-l
                        element =  aln[i].id.split('/')[0], index, l, r, s, tool
                        var.append(element)
                        s = ""
                        r = ""
                        l = 0
                else:
                    if s != "" and r != "":
                        if country != 'italy':
                            index = j - l + 1
                        else:
                            index = j-l
                        element =  aln[i].id.split('/')[0], index, l, r, s, tool                       
                        for position in range(j - l, j+1) :
                            if aln[i].seq[position].upper() != aln[0].seq[position].upper():
                                diff = find_genes(position, aln[0], aln[i])
                                if diff and not isIn(diff, diffs):
                                    diffs.append(diff)
                        var.append(element)
                        s = ""
                        r = ""
                        l = 0
    
    
    with open('Seq_differences/' + country + '.csv','w', newline = '') as out:
        csv_out = csv.writer(out)
        csv_out.writerow(['SEQ_ID','POS', 'LENGTH', 'REFERENCE', 'MUTATION', 'TOOL'])
        for row in var:
            csv_out.writerow(row)
    
    filter_differences(var, country)

def filter_differences(var, country):
    df = pd.DataFrame(var, columns = ['SEQ_ID','POS', 'LENGTH', 'REFERENCE', 'MUTATION', 'TOOL']).astype(str)
    df.drop_duplicates(subset = ['SEQ_ID','POS', 'LENGTH', 'REFERENCE', 'MUTATION'], keep="first", inplace=True)
    df.to_csv(country + '-clean.csv', index = False)
    
    

def main():
    find_variations('italy')
    find_variations('ny')
    find_variations('russia')
    find_variations('spain')
    

if __name__ == "__main__":
	main()



"""
def find_genes(variation_list, country, ref_genes):
    
    b = 0
    result = []
    
    for var in variation_list:
        if 'N' in var[3] or 'N' in var[4]:
            continue

        file = f"Results/{var[5]}-aln-{country}.fasta"
        aln = list(AlignIO.read(open(file), "fasta"))
        seq = ""
        for i in range(1, len(aln)):
            if var[0] == aln[i].id.split('/')[0]:
                seq = aln[i].seq.upper()

        b = int(var[1]) - 1

        for gene in ref_genes:
            refg = gene[2]
            if b in range(gene[0], gene[1]):
                if country != 'italy':
                    p = b - gene[0] - 1   # posizione su CDS  3037 - 266 -1? l'uno l'ho aggiunto per avere posizione 1-based
                else:
                    p = b - gene[0]
                positions = []    

                if p%3 == 0:
                    cod = refg[p-2]+refg[p-1]+refg[p]
                    #cod = ref[b-2]+ref[b-1]+ref[b] #codone originale
                    m_cod = seq[b-2]+seq[b-1]+seq[b] 
                    # Check adiacent positions
                    if refg[p-2] != seq[b-2]:
                        positions.append(b-2)
                    if refg[p-1] != seq[b-1]:
                        positions.append(b-1)
                    positions.append(b)
                
                elif p%3 == 1 and (b+2) < gene[1]: # questo b va cambiato??
                    cod = refg[p]+refg[p+1]+refg[p+2]
                    #cod = ref[b]+ref[b+1]+ref[b+2]
                    m_cod = seq[b]+seq[b+1]+seq[b+2]
                    positions.append(b)
                    if refg[p+1] != seq[b+1]:
                        positions.append(b+1)
                    if refg[p+2] != seq[b+2]:
                        positions.append(b+2)
                    
                elif p%3 == 2 and (b+1) < gene[1]: # idem, b va cambiato in p?
                    cod = refg[p-1]+refg[p]+refg[p+1]
                    #cod = ref[b-1]+ref[b]+ref[b+1]
                    m_cod = seq[b-1]+seq[b]+seq[b+1]                       
                    if refg[p-1] != seq[b-1]:
                        positions.append(b-1)
                    positions.append(b)
                    if refg[p+1] != seq[b+1]:
                        positions.append(b+1)
                
                e = var[0], list(np.asarray(positions) + 1), gene[3], cod, translate(cod), m_cod.upper(), translate(m_cod.upper()), gene[0], gene[1]
                result.append(e)
                    
        
    if result:
        #print(result)
        df = pd.DataFrame(result).astype(str)
        #df.drop_duplicates(keep="first", inplace=True)
        #df.to_csv(country + '-codons.csv', index = False)
        print(df)                                    
    

def translate(codon): 

    # codon = Seq(codon, generic_dna) # formato Seq
    # rna_cod = str(codon.transcribe()) # diventa RNA

    table = CodonTable.standard_dna_table.forward_table
    amino = table.get(codon)
    return amino

"""


"""        
def reference_genes():

    ref_genes = []
    for gb_record in SeqIO.parse("sequence.gb", "genbank"):
        ref = gb_record.seq
        for features in gb_record.features:
            if (features.type == "CDS"):
                if features.qualifiers['protein_id'][0] == 'YP_009724389.1':
                    id = features.qualifiers['db_xref'][0]
                    s = min(features.location)+1
                    e = max(features.location)+1
                    p1 = ref[s-1:13468] # -1 poiché 0 based
                    p2 = ref[13467:e]
                    g_seq = p1+p2
                    g = s,e,g_seq, id, len(g_seq)
                    ref_genes.append(g)
                else:
                    id = features.qualifiers['db_xref'][0]
                    s = min(features.location)+1
                    e = max(features.location)+1
                    g_seq = ref[s-1:e] # -1 poiché 0 based
                    g = s,e,g_seq, id, len(g_seq)
                    ref_genes.append(g)
    
    return ref_genes
"""
