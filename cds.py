import pandas as pd
import math
from Bio import GenBank, SeqIO, AlignIO, SeqFeature
from Bio.Seq import Seq
from Bio.Data import CodonTable
from Bio.Alphabet import generic_dna
import numpy as np

table = None
gb_record = None


def extract_codons_variations(variation_list: pd.DataFrame) -> list:
    """ Funzione di estrazione dei codoni e relative variazioni

    Per ogni riga del dataframe in input computa le variazioni dei codoni sulla CDS relativa alla mutazione

    :param  variation_list: Dataframe con le mutazioni nella forma
                            ['SEQ_ID','POS', 'LENGTH', 'REFERENCE', 'MUTATION', 'TOOL']
    :return Dataframe con variazioni dei codoni
    """
    __load__()
    out_list = list()
    variation_list.apply(__extract__, axis=1, out=out_list)
    df = pd.DataFrame(out_list, columns=['SEQ_ID', 'TOOL', 'POSs', 'GENE',
                                         'REF_COD', 'REF_AMMINO', 'MUT_COD', 'MUT_AMMINO', 'CDS_START', 'CDS_END']).astype(str)
    df.drop_duplicates(inplace=True)
    return df


def __load__():
    """ Funzione load

    Carica la sequenza reference in formato genbank e la tabella dei codoni
    """
    global table
    global gb_record
    table = CodonTable.standard_dna_table.forward_table
    for rec in SeqIO.parse("sequence.gb", "genbank"):
        gb_record = rec


def __extract__(row, out: list):
    """ Funzione di elaborazione di ogni singola riga

    TODO: 1. Mutazioni che "escono" dalla CDS. Soluzione spezzare length e trovare nuove pos
    TODO: 2. Inserimenti e cancellazioni nelle CDS.

    :param  row: riga elaborata
            out: lista a cui appendere i risultati
    """
    seq_id = row['SEQ_ID']
    pos = int(row['POS'])
    length = int(row['LENGTH'])
    reference = row['REFERENCE']
    mutation = row['MUTATION']
    tool = row['TOOL']

    if 'N' in reference or 'N' in mutation:
        return list()

    for feature in gb_record.features:
        if (feature.type == "CDS"):
            if (pos in feature.location):
                CDS = feature.extract(gb_record.seq)  # Estrae CDS
                subSeq = gb_record.seq[:pos - 1] + mutation + gb_record.seq[((pos - 1) + length):]  # Esegue sostituzione sul reference
                CDSm = feature.extract(subSeq)  # Estrae CDS mutata
                
                # Calcola posizione relativa
                rpos = __calculate_postion__(
                    pos , min(feature.location) ,feature.location)

                codons = __get_codons__(CDS, CDSm, rpos[0], length)
                if codons:
                    for codon in codons:
                        e = seq_id, tool, list(np.asarray(codon[2]) + min(feature.location) + 1 - rpos[1]), feature.qualifiers['db_xref'][0], codon[0], __translate__(
                            codon[0]), codon[1], __translate__(codon[1]), min(feature.location) + 1, max(feature.location) + 1
                        out.append(e)


def __get_codons__(CDS: str, CDSm: str, rpos: int, length: int) -> list:
    """ Funzione di individuazione codoni

    Estrae i codoni dalla CDS reference e dalla mutata, con posizioni di mutazione

    Attributi:
        pp: Posizione Partenza per la divisione in blocchi da 3
        lthree: Lunghezza della mutazione in multipo di 3

    :param  CDS: CDS reference
            CDSm: CDS mutata
            rpos: Posizione inizio mutazione
            length: Lunghezza mutazione
    :return Una lista contenti triple [codone reference, codone mutato, posizioni]
    """
    pp = None
    if rpos % 3 == 0:
        pp = rpos
    elif rpos % 3 == 1 and (rpos+2) < len(CDS):
        pp = rpos - 1
    elif rpos % 3 == 2 and (rpos+1) < len(CDS):
        pp = rpos - 2
    else:
        return list()

    lthree = 3 * math.ceil((rpos + length) / 3)
    codonOut = list()
    for p in range(pp, lthree, 3):
        positions = list()
        if rpos <= p < rpos+length:
            positions.append(p)
        if rpos <= p + 1 < rpos+length:
            positions.append(p + 1)
        if rpos <= p + 2 < rpos+length:
            positions.append(p + 2)
        codonOut.append([str(CDS[p:p+3]), str(CDSm[p:p+3]), positions])

    return codonOut


def __calculate_postion__(pos: int, cds_start: int, location: SeqFeature.CompoundLocation) -> list:
    """ Funzione di calcolo posizione

    Computa la corretta posizione per individuare l'inizio della mutazione

    Attributi:
        pos: Posizione
        joins: Numero di posizioni aggiunte dalla join della SeqFeature.CompoundLocation

    :param  pos: Posizione base 1 sulla reference
            cds_start: Posizione di inizio CDS, base 0 (i.e. min(SeqFeature.location))
            location: SeqFeature.location
    :return Una lista in cui il primo valore corrisponde alla posizione sulla CDS, il secondo 
            al numero di join
    """
    joins = 0
    max_last_join = 0
    for fl in location.parts:
        if max_last_join != 0:
            joins += (max_last_join - min(fl))
        if pos in fl:
            return [pos + joins - 1 - cds_start, joins]
        else:
            max_last_join = max(fl)


def __translate__(codon):
    """ Funzione translate

    Traduce un codone nell'aminoacido corrispondente
    
    :param  codon: Seq codone
    :return ammino: amminoacido
    """

    amino = table.get(codon)
    return amino
