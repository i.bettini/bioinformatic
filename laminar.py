import pandas as pd
import random
import datetime
import numpy as np

def IsLaminar(df: pd.DataFrame):
    L = df.copy()
    L.apply(__fill_row__, axis=1)

    for col in L.columns:
        pv = 0
        first = True
        for value in L[col].values:
            if value != 0 and first:
                pv = value
                first = False
            elif value != 0:
                if value != pv:
                    return False
    return True


def __fill_row__(row):
    k = -1
    j = 0
    for char in row.index:
        if row[char] == 1:
            row[char] = k
            k = j
        j += 1
    return row

