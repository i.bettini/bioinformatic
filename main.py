import Bio
from Bio import GenBank, SeqIO, AlignIO
from Bio.Seq import Seq
from Bio.Data import CodonTable
from Bio.Alphabet import generic_dna
import csv
import glob
import pandas as pd
import os
import pdb
import numpy as np
import cds
import phylo
import laminar as lr


def find_variations(country):
    """ Funzione che individua le variazioni rispetto al reference della sequenza in esame

    Dato un paese in input, analizza gli allineamenti generati dai tre tool Clustal Omega, MAFFT, Kalign e individua
    le variazioni presenti

    :param  country: paese del quale considerare l'allineamento

    :return lista delle variazioni presenti senza doppioni
    """

    s = []
    r = []
    l = 0
    var = []
    url = 'Results/*' + country + '.fasta'

    for f in glob.glob(url):
        tool = os.path.split(f)[-1].split("-")[0]
        aln = list(AlignIO.read(open(f), "fasta"))
        for i in range(1, len(aln)):
            for j in range(len(aln[i].seq)):
                if aln[i].seq[j].upper() != aln[0].seq[j].upper():
                    s += aln[i].seq[j].upper()
                    r += aln[0].seq[j].upper()
                    l += 1
                    if (j == len(aln[i].seq)-1):
                        if country != 'italy' or country == 'horizontal':
                            index = j - l + 1
                        else:
                            index = j-l
                        element = aln[i].id.split('/')[0], index, l, r, s, tool
                        var.append(element)
                        s = ""
                        r = ""
                        l = 0
                else:
                    if s != "" and r != "":
                        if country != 'italy':
                            index = j - l + 1
                        else:
                            index = j-l
                        element = aln[i].id.split('/')[0], index, l, r, s, tool
                        var.append(element)
                        s = ""
                        r = ""
                        l = 0

    """
    with open('Seq_differences/' + country + '.csv','w', newline = '') as out:
        csv_out = csv.writer(out)
        csv_out.writerow(['SEQ_ID','POS', 'LENGTH', 'REFERENCE', 'MUTATION', 'TOOL'])
        for row in var:
            csv_out.writerow(row)
    """

    clean_variations = pd.DataFrame(var, columns=[
                                    'SEQ_ID', 'POS', 'LENGTH', 'REFERENCE', 'MUTATION', 'TOOL']).astype(str)
    clean_variations.drop_duplicates(
        subset=['SEQ_ID', 'POS', 'LENGTH', 'REFERENCE', 'MUTATION'], keep="first", inplace=True)
    #df.to_csv(country + '-clean.csv', index = False)

    return clean_variations


def characters_matrix(df: pd.DataFrame, graph_name: str):
    """ Funzione di creazione della matrice binaria dei caratteri

    Date le variazioni individuate genera una matrice binaria dei caratteri, dove 1/0 = c’è/non c’è il carattere, 
    verifica la laminarità di tale matrice e chiama la funzione per la costruzione dell'albero filogenetico.

    :param  df: Dataframe contenente le variazioni individuate
    """

    # Trasformo il dataframe in lista per ottenere nome delle righe (id delle sequenze) e delle colonne (posizioni delle variazioni)
    dfToList = list(df.itertuples(index=False, name=None))
    rows = []  # sequences
    col = []  # characteristics
    for i in dfToList:
        if i[0] not in rows:
            rows.append(i[0])
        if i[2] not in col:
            col.append(i[2])

    # Genero la matrice binaria dei caratteri
    m = np.zeros((len(rows), len(col)))

    for tup in dfToList:
        for i in range(len(rows)):
            for j in range(len(col)):
                if rows[i] == tup[0] and col[j] == tup[2]:
                    m[i][j] = 1

    mtx = pd.DataFrame(m)

    mtx.columns = col
    mtx.index = rows
    # Ordino per numero di 1 decrescente
    tmp_mtx = mtx.transpose()
    ordered_cols = tmp_mtx.sum(axis=1).sort_values(ascending=False).index
    tmp_mtx = tmp_mtx.reindex(ordered_cols)
    mtx = tmp_mtx.transpose()

    # Costruisco la matrice L
    L = np.zeros((len(rows), len(col)))

    for i in range(len(rows)):  # per ogni riga
        k = -1
        for j in range(len(col)):  # per ogni colonna
            if mtx.iat[i, j] == 1:
                L[i][j] = k
                k = j

    ldf = pd.DataFrame(L)
    ldf.columns = mtx.columns
    ldf.index = mtx.index
    t_ldf = ldf.transpose()
    # Trasformo L in una lista dove le righe sono le colonne del Dataframe per controllare più facilmente la laminarità
    l_ldf = list(t_ldf.itertuples(index=True, name=None))

    Laminar = True
    to_del = []
    for i in l_ldf:
        if (len(set(i[1:])) > 2):  # con set ho solo gli elementi unici, se sono più di 2 vuol dire che Lij != Llj per alcuni i, l e sono entrambi diversi da 0
            Laminar = False
            to_del.append(i[0])

    if not Laminar:
        for i in range(len(to_del)):
            mtx = mtx.drop(to_del[i], 1)
    mtx.loc['0_NC_045512'] = [0] * len(mtx.columns)
    mtx.sort_index(inplace=True)
    phylo.phylo_tree(mtx, graph_name)


def main():

    comparisons = ['italy', 'ny', 'russia', 'spain', 'horizontal']
    
    for compare in comparisons:

        print(f"Precessing {compare}...")

        # Trovo variazioni
        variations = find_variations(compare)
        print(f"\tVariations OK...")
        # Trovo codoni e aminoacidi corrispondenti
        codons = cds.extract_codons_variations(variations)
        print(f"\tCodons OK...")
        # Esporto i risultati in csv
        codons.to_csv(f"./Codons/{compare}-codons.csv", index=False)
        print(f"\tCSV exported...")
        # Costruisco l'albero di filogenesi perfetta
        characters_matrix(codons, compare)
        print(f"\tTree OK...")

        print(f"{compare} finished!\n")


if __name__ == "__main__":
	main()
