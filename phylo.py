import pandas as pd
from anytree import Node, RenderTree, search, NodeMixin, PreOrderIter
from anytree.exporter import DotExporter
from collections import OrderedDict
import pydot
import fileinput


class WNode(NodeMixin):

    """ Weighted Node class

    https://anytree.readthedocs.io/en/latest/tricks/weightededges.html

    """

    def __init__(self, foo, parent=None, weight=None, visual_name=None):
        super(WNode, self).__init__()
        self.name = foo
        self.parent = parent
        self.weight = weight if parent is not None else None
        self.visual_name = foo if visual_name is None else visual_name

    def _post_detach(self, parent):
        self.weight = None


def gen_phylo(lM: pd.DataFrame) -> list:
    """ Generazione albero di filogenesi perfetta

    Genera l'albero di filogenesi perfetta partendo da una matrice laminare

    :param  lm: Dataframe rappresentate la matrice laminare
    :return lista contenente la radice dell'albero (per comodità) ed un OrderedDict contentente tutti i nodi dell'albero
            (radice compresa)
    """

    nodes = OrderedDict()  # Dictionary contenente tutti i nodi dell'albero
    root = WNode("0" * len(lM.columns))  # Creazione radice
    nodes[root.name] = root

    lM.apply(__gen_nodes__, axis=1, nodes=nodes, root=root)

    return (root, nodes)


def __gen_nodes__(row: pd.Series, nodes: OrderedDict, root: WNode):
    """ Elaborazione riga matrice

    :param  row: riga del dataframe
            nodes: OrderedDict contenente i nodi dell'albero
            root: radice dell'albero
    """

    current_node = root
    name = "0" * len(row.index)
    index = 0

    for char in row.index:
        if row[char] == 1:
            name = name[:index] + '1' + name[index+1:]
            visual_name = f"{name[:index]}<font color='red'><B>1</B></font>{name[index+1:]}"
            u = search.findall(
                current_node, filter_=lambda node: node.weight == char)
            if u:
                current_node = u[0]
            else:
                nodes[name] = WNode(name, parent=current_node,
                                    weight=char, visual_name=visual_name)
                current_node = nodes[name]
        index += 1

    nodes[row.name] = WNode(row.name, parent=current_node, weight='')


def export_image(tree: list, phylo_matrix: pd.DataFrame, graph_name: str):

    # Crezione albero in formato dot con funzione di anytree
    export = DotExporter(tree[0], nodenamefunc=lambda node: node.name, nodeattrfunc=lambda node: f"label=<{node.visual_name}>", edgeattrfunc=lambda parent,
                         child: f"style=bold,label=\"{child.weight[1:-1]}\"")
    export.to_dotfile(f"./Phylotree/Perfect/Full/{graph_name}.dot")

    # Postprocessing dell'albero

    # Rank
    nomiSeq = list(phylo_matrix.index.values)
    nomiStr = " ".join([f"\"{name}\";" for name in nomiSeq])
    rankStr = f"\t{{rank = same; {nomiStr}}}"
    for line in fileinput.FileInput(f"./Phylotree/Perfect/Full/{graph_name}.dot", inplace=1):
        if "}" in line:
            line = line.replace(line, rankStr+"\n}")
        print(line, end='')

    # Dot to image
    (graph,) = pydot.graph_from_dot_file(
        f"./Phylotree/Perfect/Full/{graph_name}.dot")
    graph.write_png(f"./Phylotree/Perfect/Full/{graph_name}.png")


def phylo_tree(phylo_matrix: pd.DataFrame, graph_name=""):

    tree = gen_phylo(phylo_matrix)
    export_image(tree, phylo_matrix, graph_name)
    newick = f"{convert_tree(tree[0])};"
    with open(f"./Phylotree/Perfect/Newick/{graph_name}.tree", "w") as file:
        file.write(newick)


def convert_tree(nod: WNode) -> str:
    strNode = ""
    if nod.is_leaf:
        # Ho trovato una foglia
        strNode = f"{nod.name}"
    else:
        if len(nod.children) == 1:
            strNode = f"{convert_tree(nod.children[0])}"
        else:
            strNode += "("
            for child in nod.children:
                strNode += f"{convert_tree(child)},"
            strNode = strNode[:-1]
            strNode += f")"
    return strNode
